//
//  ViewController.m
//  dropDown2
//
//  Created by Click Labs135 on 10/6/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *arrName;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;

@end

@implementation ViewController
@synthesize submitButton;
@synthesize  tableData;
@synthesize  deleteBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    arrName=[[NSMutableArray alloc]init];
    [submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn addTarget:self action:@selector(deleteRow:) forControlEvents:UIControlEventTouchUpInside];
    tableData.hidden=YES;
    deleteBtn.hidden=YES;
    arrName=@[@"vijay",@"tamanna",@"pooja",@"suchi",@"shiv",@"koshal",@"akshda",@"damini",@"garima",@"ujjwal",@"shreya",@"rahil"];
    
}
-(void)submitButtonPressed:(UIButton *)sender
{
    tableData.hidden=NO;
    deleteBtn.hidden=NO;
}
-(void)deleteRow:(UIButton *)sender{
    [tableData setEditing:YES animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrName.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    cell.textLabel.text=arrName[indexPath.row];
    cell.backgroundColor=[UIColor grayColor];
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [submitButton setTitle:arrName[indexPath.row] forState:UIControlStateNormal];
    tableData.hidden=YES;
    deleteBtn.hidden=YES;
    

    
}




-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(editingStyle==UITableViewCellEditingStyleDelete)
        
    {
        
        [arrName removeObjectAtIndex:indexPath.row];
        
        [tableData deleteRowsAtIndexPaths:[NSMutableArray arrayWithObjects:indexPath, nil ] withRowAnimation:UITableViewRowAnimationFade];
        [tableData reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
